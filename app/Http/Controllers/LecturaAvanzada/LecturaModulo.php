<?php

namespace App\Http\Controllers\LecturaAvanzada;

use App\Models\Biblioteca\BibliotecaLibro;
use App\Models\Biblioteca\BibliotecaLibroCabecera;
use App\Models\Tienda\Libro\LibroProducto;
use App\Models\Biblioteca\BibliotecaRevista;
use App\Models\Biblioteca\BibliotecaRevistaCabecera;
use App\Models\ContenidoAvanzado\ContenidoAvanzadoCabeceraV2;
use App\Models\ContenidoAvanzado\ContenidoAvanzadoV2;
use App\Models\Jurisprudencia\Jurisprudencia;
use App\Models\Libros\LibroReferencia;
use App\Models\Norma\NormaPe;
use App\Models\Revista\RevistaReferencia;
use App\Models\Tienda\ContenidoAvanzado\ContenidoAvanzadoProducto;
use App\Models\Tienda\Revista\RevistaProducto;
use Illuminate\Database\Eloquent\Collection;
use stdClass;

class LecturaModulo
{

    public $modulos = [
        'AV' => [
            'model' => ContenidoAvanzadoV2::class,
            'cabecera' => ContenidoAvanzadoCabeceraV2::class,
            'arbol' => true,
            'ruta_img' => 'assets/uploads/contenido-avanzado/',
            'campo' => 'contenido_avanzado_id',
            'modeloProducto' => ContenidoAvanzadoProducto::class

        ],
        'LI' => [
            'model' => BibliotecaLibro::class,
            'cabecera' => BibliotecaLibroCabecera::class,
            'arbol' => true,
            'ruta_img' => 'assets/uploads/biblioteca/Libros/',
            'campo' => 'libro_id',
            'modeloProducto' => LibroProducto::class,
            'modeloReferencia' => LibroReferencia::class,
        ],
        'RV' => [
            'model' => BibliotecaRevista::class,
            'cabecera' => BibliotecaRevistaCabecera::class,
            'arbol' => true,
            'ruta_img' => 'assets/uploads/biblioteca/Revistas/',
            'campo' => 'revista_id',
            'modeloProducto' => RevistaProducto::class,
            'modeloReferencia' => RevistaReferencia::class
        ],
        'NL' => [
            'model' => NormaPe::class,
            'cabecera' => null,
            'arbol' => false,
            'ruta_img' => '',
            'campo' => ''
        ],
        'JR' => [
            'model' => Jurisprudencia::class,
            'cabecera' => null,
            'arbol' => false,
            'ruta_img' => 'assets/uploads/jurisprudencias/',
            'campo' => ''
        ],
    ];


    public $aliasModulos = [
        'libro' => 'LI',
        'revista' => 'RV',
        'tienda' => 'TI'
    ];

    public $modulosCollection;


    public function __construct()
    {
        $this->convertToCollection();
    }

    public function getModuloLectura($alias)
    {
        $modulo = $this->modulos[$alias];

        $object =  new stdClass();
        $object->model = $modulo['model'];
        $object->cabecera = $modulo['cabecera'];
        $object->arbol = $modulo['arbol'];
        $object->ruta_img = $modulo['ruta_img'];
        $object->campo = $modulo['campo'];
        $object->modeloProducto = $modulo['modeloProducto'];
        $object->modeloReferencia = $modulo['modeloReferencia'];

        return $object;
    }

    public function getAliasModulo($nombre)
    {
        return $this->aliasModulos[$nombre];
    }


    public function convertToCollection()
    {
        $modulosCollection = collect([]);

        foreach ($this->modulos as $modulo) {
            $modulosCollection->add($modulo);
        }

        $this->modulosCollection = $modulosCollection;
    }
}
