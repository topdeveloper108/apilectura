<?php

namespace App\Http\Controllers\LecturaAvanzada;

class ClearContenido
{

    private $tags = [
        ['tag' => 'p', 'html'  => '<p>', 'replace' => '<p>'],
        ['tag' => 'li', 'html' => '<li>', 'replace' => '<li>'],
        ['tag' => 'ul', 'html' => '<ul>', 'replace' => '<ul>'],
        ['tag' => 'ol', 'html' => '<ol>', 'replace' => '<ol>'],
        ['tag' => 'u', 'html'  => '<u>', 'replace'   => '<u>'],
        ['tag' => 'blockquote', 'html' => '<blockquote>', 'replace' => '<blockquote>'],
        ['tag' => 'div', 'html' => '<div>', 'replace' => '<div>'],
        ['tag' => 'span', 'html' => '<span>', 'replace' => '<b>'],
        // ['tag' => 'img', 'html' => '<img>', 'replace' => '<strong><p>AQUI VA UNA IMAGEN </p></strong>'],
        ['tag' => 'a', 'html' => '<a>', 'replace'  => ''],
        ['tag' => 'h1', 'html' => '<h1>', 'replace' => '<h1>'],
        ['tag' => 'h2', 'html' => '<h1>', 'replace' => '<h2>'],
        ['tag' => 'h3', 'html' => '<h1>', 'replace' => '<h3>'],
        ['tag' => 'h4', 'html' => '<h1>', 'replace' => '<h4>'],
        ['tag' => 'h5', 'html' => '<h1>', 'replace' => '<h5>'],
        ['tag' => 'hr', 'html' => '<hr>', 'replace' => ''],
    ];

    private $allowedTagsStyle = ["p", "h1", "h2", "h3"];


    public function clearText($text)
    {

        $textClear = preg_replace($this->getExpresiones(), $this->getReplace(), htmlspecialchars_decode(html_entity_decode($text)));
        preg_match_all('/(<\/?\w+>)\1/', $textClear, $resultado, PREG_PATTERN_ORDER);

        $coincidencias = collect($resultado[0])->unique()->sort()->values();
        $duplicadas = [];
        $reemplazo = [];

        for ($i = 0; $i < count($coincidencias); $i++) {
            $tag = explode('>', $coincidencias[$i]);
            array_push($duplicadas, $coincidencias[$i]);
            array_push($reemplazo, "{$tag[0]}>");
        }

        $textClear = str_replace($duplicadas, $reemplazo, $textClear);

        return $textClear;
    }


    public function getChildContent(&$contenido, $childrens, $isArray = false)
    {

        $childrens->map(function ($child) use (&$contenido, $isArray) {
            $depth = $child->depth + 1;
            $titulo = "<h{$depth} id='c-{$child->id}'>" . $child->titulo . "</h{$depth}>";
            $contenido .= $titulo;
            $contenido .= $this->clearText($isArray ? @$child->contenidos_nuevo  : @$child->contenidos->contenido);
            $this->getChildContent($contenido, $child->children, $isArray);
        });
    }

    private function getExpresiones()
    {

        $arr = ['/<table.*table>/', '/<\/span>/', '/<\/a>/'];

        for ($i = 0; $i < count($this->tags); $i++) {
            if (!in_array($this->tags[$i]['tag'], $this->allowedTagsStyle)) {
                array_push($arr, $this->getExpresion($this->tags[$i]['tag']));
            }
        }

        return $arr;
    }

    private function getReplace()
    {

        // REPLACE TABLE <br><strong><p> AQUI VA UNA TABLA</p></strong><br>

        $arr = ['', '</b>', ''];

        for ($i = 0; $i < count($this->tags); $i++) {
            if (!in_array($this->tags[$i]['tag'], $this->allowedTagsStyle)) {
                array_push($arr, $this->tags[$i]['replace']);
            }
        }

        return $arr;
    }

    private function getTagsHtml()
    {
        $tags = '<table><thead><tbody><tr><th><td><h1><h2><h3><h4><h5><h6>';
        for ($i = 0; $i < count($this->tags); $i++) {
            $tags .= $this->tags[$i]['html'];
        }
        return $tags;
    }

    private function getExpresion($tag)
    {
        $expresion = '/<' . $tag . '\s?(((\w+)((\W\w+)?){1,}=[\"\']([\w\/\s.\"\',:#%();-]+)?[\"\']\s?)?){1,}>/';
        return $expresion;
    }

    public function enlazarImg(&$contenido)
    {
        preg_match_all($this->getExpresion('img'), htmlspecialchars_decode(html_entity_decode($contenido)), $coincidencias, PREG_PATTERN_ORDER);

        $resultados = collect($coincidencias[0])->unique()->sort()->values();
        $imagenes = [];

        foreach ($resultados as $coincidencia) {
            $enlace =  explode('class="', $coincidencia);

            $srcArray = explode('src=',$enlace[0]);

            $srcArray2 =explode('"',$srcArray[1]);

            $src = $srcArray2[1];
            
            // $ahref = "a class='enlace-imagen' href='{$src}' target='_blank'>Visualice la tabla. Clic aquí</a";
            $ahref = "img src='http://siip3dev.institutopacifico.pe/assets/uploads/biblioteca/Libros/froala/51nAOEx1NHzsv9SvoCqMsQTWjp1Xo3WyOwHtXzID.png' width='370' height='200' /";
            $contenido = preg_replace($coincidencia, $ahref, $contenido);
            array_push($imagenes,$ahref);
        }

        return $imagenes;
    }
}
