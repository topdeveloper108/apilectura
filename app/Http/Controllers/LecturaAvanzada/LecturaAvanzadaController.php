<?php

namespace App\Http\Controllers\LecturaAvanzada;

use App\Http\Controllers\LecturaAvanzada\ClearContenido;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Productos\Strategies\ModuloStrategy;
use App\Models\Lectura\LecturaMarcador;

class LecturaAvanzadaController extends Controller
{

    private $modulo;
    public $postData;

    public function __construct()
    {
        $this->middleware('auth:api')->except(['getContenido']);
    }

    public function getContenido(Request $request, $module, $slug)
    {


        try {

            $lecturaModulo = new LecturaModulo();
            $alias = $lecturaModulo->getAliasModulo($module);
            $muestra = false;

            $this->modulo = $lecturaModulo->getModuloLectura($alias);
            $model = $this->modulo->model;
            $rutaImg = $this->modulo->ruta_img;
            $campo = $this->modulo->campo;

            $item = $model::whereSlug($slug)->orWhere('id', $slug)->first();

            if (!$item) {
                throw new \Exception("Item no encontrado", 404);
            }

            $id = $item->id;

            $isArray = $alias == 'AV';

            $clearContent = new ClearContenido();

            if ($this->modulo->arbol) {

                $cabecera = $this->modulo->cabecera;

                $cabeceras = $cabecera::with('contenidos')->where($campo, $id)
                    ->where(function ($q) use ($muestra) {
                        return $muestra ? $q->where('muestra_gratis', $muestra) : $q;
                    })->orderBy('orden', 'ASC')->get();

                if ($cabeceras->isEmpty() && $muestra) {
                    throw new \Exception("Este producto no posee muestra gratuita", 404);
                }

                $cabeceras = $cabeceras->toHierarchy();
                $contenido = '';

                foreach ($cabeceras as $cabecera) {

                    $titulo = "<h1 id='c-{$cabecera->id}'>{$cabecera->titulo}</h1>";
                    $contenido .=  $titulo;

                    if ($isArray) {
                        $this->formatContenidoArray($cabecera);
                        $this->getChildContentFormat($cabecera->children);
                    }

                    $contenido .=  $clearContent->clearText($isArray ? $cabecera->contenidos_nuevo : @$cabecera->contenidos->contenido);
                    $clearContent->getChildContent($contenido, $cabecera->children, $isArray);
                }

                $item->indice = $this->getIndice($cabecera, $campo, $id);
            } else {
                $contenido = $clearContent->clearText(@$item->contenido);
            }

            $clearContent->enlazarImg($contenido);
            $item->imagen =  config('globals.base_url_production') . "{$rutaImg}{$item->imagen}_original.png";
            $item->contenido = $contenido;

            return response()->json($item);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 422);
        }
    }


    public function getIndice($cabecera, $campo, $id)
    {

        $cabeceras = $cabecera::where($campo, $id)->where('depth', '<', '2')->where('articulo', 1)->orderBy('orden', 'ASC')->get();
        $cabeceras = $cabeceras->toHierarchy();
        $indice = collect();

        foreach ($cabeceras as $cabecera) {
            $titulo = "<h1 id='i-{$cabecera->id}'>{$cabecera->titulo}</h1>";
            $indice->add(["titulo" => $titulo, 'indice' => null, 'id' => $cabecera->id]);
            $this->getChildIndice($cabecera->children, $indice);
        }

        return $indice;
    }


    private function getChildIndice($childrens, &$indice)
    {
        $childrens->map(function ($child) use (&$indice) {
            $depth = $child->depth + 1;
            $titulo = "<h{$depth} id='i-{$child->id}'>" . $child->titulo . "</h{$depth}>";
            $indice->add(["titulo" => $titulo, 'indice' => null, 'id' => $child->id]);
            $this->getChildIndice($child->children, $indice);
        });
    }


    private function formatContenidoArray(&$cabecera)
    {

        $content = '';

        foreach ($cabecera->contenidos as $c) {
            $content .= @$c->contenido;
        }

        $cabecera->contenidos_nuevo = $content;
    }

    private function getChildContentFormat(&$childrens)
    {

        foreach ($childrens as $child) {
            $this->formatContenidoArray($child);
            if (isset($child->children) && !($child->children->isEmpty())) {
                $this->getChildContentFormat($child->children);
            }
        }
    }
}
