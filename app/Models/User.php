<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Traits\Uuids;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, Uuids;

    protected $table = 'usuarios_maestro';

    public $timestamps = false;

    public $incrementing = false;

    protected $fillable = [
        'id', 
        'tipo_usuario', 
        'estado', 
        'email', 
        'password',
        'sesiones_permitidas',
        'sesiones_totales',
        'fecha_nacimiento',
        'remenber_token',
        'fecha_registro',
    ];

    protected $hidden = [
        'password', 'remenber_token'
    ];

    /* 
    |-----------------------------------------------------------
    |   PERMISOS DE USUARIOS
    |-----------------------------------------------------------
    */

    public function checkPermissions($permissions) 
    {
        if (is_array($permissions)) {
            if ($permissions[0] == 'and') {
                $count = 0;
                $arrayLength = count($permissions);
                foreach ($permissions as $key => $permission) {
                    if ($key) {
                        if (in_array($permission, $this->getPermissions())) {
                            $count++;
                        }
                    }
                }
                return $count === ($arrayLength - 1); // return true or false
            } elseif($permissions[0] == 'or') {
                foreach ($permissions as $key => $permission) {
                    if ($key) {
                        if (in_array($permission, $this->getPermissions())) {
                            return true; // return true or false
                        }
                    }
                }
            }
        } else {
            return in_array($permissions, $this->getPermissions()); // return true or false
        }
    }

    public function getPermissions()
    {
        // GET PERMISSIONS
        $permissions = [];
        foreach(array_pluck($this->permissions, 'codigo') as $permission) {
            array_push($permissions, $permission);
        }
        return $permissions;
    }

    public function permissions() 
    {
        return $this->belongsToMany('App\Models\Permission', 'usuarios_permisos', 'usuario_id', 'permiso_id')
        ->withPivot('id');
    }

    /* 
    |-----------------------------------------------------------
    |   SESIONES
    |-----------------------------------------------------------
    */

    public function getSessions()
    {
        return $this->sessions()->count();
    }
    
    public function allowedSessions()
    {
        return $this->sesiones_permitidas;
    }

    public function checkLimitSessions()
    {
        return $this->getSessions() >= $this->allowedSessions();
    }

    public function sessions() 
    {
        return $this->hasMany('App\Models\Session', 'usuario_id');
    }

    /* 
    |-----------------------------------------------------------
    |   TIPO DE USUARIO
    |-----------------------------------------------------------
    */

    public function isOperator()
    {
        return ($this->tipo_usuario == 0) ? true : false;
    }

    public function isSeller()
    {
        return ($this->tipo_usuario == 2) ? true : false;
    }

    public function isPointOfSale()
    {
        return ($this->tipo_usuario == 1) ? true : false;
    }

    public function siip_operator()
    {
        return $this->hasOne('App\Models\SiipOperator', 'usuario_id');
    }

    public function siip_seller()
    {
        return $this->hasOne('App\Models\SiipSeller', 'usuario_id');
    }

    public function siip_point_of_sale()
    {
        return $this->hasOne('App\Models\SiipPointOfSale', 'usuario_id');
    }

    public function details()
    {
        if ($this->isOperator()) {
            return $this->siip_operator();
        }
        if ($this->isSeller()) {
            return $this->siip_seller();
        }
        if ($this->isPointOfSale()) {
            return $this->siip_point_of_sale();
        }
    }

    public function talleres()
    {
        return $this->hasMany('App\Models\Videoteca\VideotecaTaller', 'usuairo_id');
    }

    /* 
    |-----------------------------------------------------------
    |   HISTORIAL
    |-----------------------------------------------------------
    */

    public function history()
    {
        return $this->hasMany('App\Models\SiipHistory', 'usuario_id');
    }
    // public function history()
    // {
    //     return $this->hasMany('App\SiipUserHistory', 'usuario_id');
    // }

    /* 
    |-----------------------------------------------------------
    |   STAFF
    |-----------------------------------------------------------
    */
    public function staff()
    {
        return $this->hasOne('App\Models\Staff', 'usuario_id');
    }
    /* 
    |-----------------------------------------------------------
    |   HISTORIAL CAMBIOS
    |-----------------------------------------------------------
    */
    public function changeHistory()
    {
        return $this->hasMany('App\Models\UserChangeHistory', 'modificado_id');
    }

    public function resourcesHistory()
    {
        return $this->hasMany('App\Models\ResourceHistory', 'usuario_id');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

     /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}

