<?php

namespace App\Models\Biblioteca;

use App\Models\Libros\LibroReferencia;
// use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Baum\Node;

class BibliotecaLibroCabecera extends Node
{
    // use Uuids;

    protected $table = 'biblioteca_libros_cabeceras';

    public $timestamps = false;

    public $incrementing = false;

    protected $orderColumn = 'orden';

    protected $casts = [
        'muestra' => 'boolean',
        'expandido' => 'boolean',
    ];

    protected $fillable = [
        'parent_id',
        'libro_id',
        'orden',
        'titulo',
        'subtitulo',
        'muestra',
        'expandido'
    ];


    public function libro()
    {
        return $this->belongsTo('App\Models\Biblioteca\BibliotecaLibro', 'libro_id');    
    }

    public function contenidos()
    {
        return $this->hasOne('App\Models\Biblioteca\BibliotecaLibroContenido', 'biblioteca_libro_cabecera_id');
    }
    
    public function contents()
    {
        return $this->hasMany('App\Models\Biblioteca\BibliotecaLibroContenido', 'biblioteca_libro_cabecera_id');
    }

    public function referencias(){
        return $this->hasMany(LibroReferencia::class,'cabecera_id');
    }


}
