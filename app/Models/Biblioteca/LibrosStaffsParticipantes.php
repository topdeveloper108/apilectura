<?php

namespace App\Models\Biblioteca;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class LibrosStaffsParticipantes extends Model
{
    // use Uuids;

    protected $table = 'libros_staff_participantes';
    public $timestamps = false;
    public $incrementing = false;
    protected $fillable = [
        'libro_id',
        'staff_id',
        'staff_categoria_id',
    ];

    public function categoria () {
        return $this->belongsTo('App\Models\Biblioteca\LibrosStaffsCategorias', 'staff_categoria_id');
    }
    
    public function staff () {
        return $this->belongsTo('App\Models\Staff',  'staff_id');
    }

}
