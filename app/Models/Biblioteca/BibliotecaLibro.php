<?php

namespace App\Models\Biblioteca;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Favoriteable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;


class BibliotecaLibro extends Model
{

    use Uuids;

    use Favoriteable;

    use SoftDeletes;

    protected $table = 'biblioteca_libros';

    protected $casts = [
        'destacado' => 'boolean',
    ];

    protected $dates = ['fecha_eliminacion'];

    const DELETED_AT = 'fecha_eliminacion';


    public $timestamps = false;

    public $incrementing = false;

    protected $fillable = [
        'plan_id',
        'sku_id',
        'categoria_id',
        'titulo',
        'subtitulo',
        'anio_publicacion',
        'estado',
        'tipo',
        'imagen',
        'tapa',
        'paginas',
        'formato',
        'color1',
        'color2',
        'vistas',
        'fecha_creacion'
    ];

    public function cabeceras()
    {
        return $this->hasMany('App\Models\Biblioteca\BibliotecaLibroCabecera', 'libro_id');
    }

    public function contents()
    {
        return $this->hasMany('App\Models\Biblioteca\BibliotecaLibroContenido', 'biblioteca_libro_id');
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\Biblioteca\BBLibroCategoria', 'categoria_id');
    }
}
