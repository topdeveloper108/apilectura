<?php

namespace App\Models\Biblioteca;
// use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class LibrosStaffsCategorias extends Model
{
    // use Uuids;

    protected $table = 'libros_staffs_categorias';
    public $timestamps = false;
    public $incrementing = false;
    protected $fillable = [
        'libro_id',
        'nombre_categoria',
        'estado',
    ];

}
