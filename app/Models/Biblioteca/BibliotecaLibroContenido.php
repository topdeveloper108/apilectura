<?php

namespace App\Models\Biblioteca;

// use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Baum\Node;

class BibliotecaLibroContenido extends Node
{
    // use Uuids;

    protected $table = 'biblioteca_libros_contenidos';

    public $timestamps = false;

    public $incrementing = false;

    protected $orderColumn = 'orden';


    protected $fillable = [
        'parent_id',
        'orden',
        'biblioteca_libro_cabecera_id',
        'biblioteca_libro_id',
        'contenido',
    ];

    
    public function cabecera()
    {
        return $this->belongsTo('App\Models\Biblioteca\BibliotecaLibroCabecera', 'biblioteca_libro_cabecera_id');
    }

    public function libro()
    {
        return $this->belongsTo('App\Models\Biblioteca\BibliotecaLibro', 'biblioteca_libro_id');    
    }
}
