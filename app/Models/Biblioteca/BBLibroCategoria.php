<?php

namespace App\Models\Biblioteca;

use Illuminate\Database\Eloquent\Model;

class BBLibroCategoria extends Model
{
    protected $table = 'biblioteca_libros_categorias';

    public $timestamps = false;

    protected $fillable = [
        'nombre',
        'alias'
    ];
}
