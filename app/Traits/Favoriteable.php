<?php

namespace App\Traits;

use App\Models\Cliente\ClienteFavorito;
use Illuminate\Support\Facades\Auth;

trait Favoriteable
{
	/**
     * Define a polymorphic one-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
	public function favorites()
	{
		return $this->morphMany(ClienteFavorito::class, 'favorito');
	}
	/**
	 * Add this Object to the user favorites
	 * 
	 * @param  int $cliente_id  [if  null its added to the auth user]
	 */
	public function addFavorite($cliente_id = null)
	{
		$favorite = new ClienteFavorito(['cliente_id' => ($cliente_id) ? $cliente_id : Auth::id()]);
		$this->favorites()->save($favorite);
	}
	/**
	 * Remove this Object from the user favorites
	 *
	 * @param  int $cliente_id  [if  null its added to the auth user]
	 * 
	 */
	public function removeFavorite($cliente_id = null)
	{
		$this->favorites()->where('cliente_id', ($cliente_id) ? $cliente_id : Auth::id())->delete();
	}
	/**
	 * Toggle the favorite status from this Object
	 * 
	 * @param  int $cliente_id  [if  null its added to the auth user]
	 */
	public function toggleFavorite($cliente_id = null)
	{
		$this->isFavorited($cliente_id) ? $this->removeFavorite($cliente_id) : $this->addFavorite($cliente_id) ;
	}
	/**
	 * Check if the user has favorited this Object
	 * 
	 * @param  int $cliente_id  [if  null its added to the auth user]
	 * @return boolean
	 */
	public function isFavorited($cliente_id = null)
	{
		return $this->favorites()->where('cliente_id', ($cliente_id) ? $cliente_id : Auth::id())->exists();
	}
	/**
     * Return a collection with the Users who marked as favorite this Object.
     * 
     * @return Collection
     */
	public function favoritedBy()
	{
		return $this->favorites()->with('user')->get()->mapWithKeys(function ($item) {
            return [$item['user']];
        });
	}
	/**
	 * Count the number of favorites
	 * 
	 * @return int
	 */
	public function getFavoritesCountAttribute()
	{
		return $this->favorites()->count();
	}
	/**
	 * @return favoritesCount attribute
	 */
	public function favoritesCount()
	{
		return $this->favoritesCount;
	}
}